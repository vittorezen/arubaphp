<?php
require __DIR__ . '/../src/ArubaPhp/ArubaPhp.php';
require __DIR__ . '/../src/ArubaPhp/Client/AbstractClient.php';
require __DIR__ . '/ClientCurlMockup.php';

use \ArubaPhp\ArubaPhp;

class ArubaPhpTest extends \PHPUnit_Framework_TestCase
{


    public function testConstructor()
    {
        $arubaPHP = $this->setupClass();
        return $this->assertNotEmpty($arubaPHP);
    }

    public function testGetPresents()
    {
        $arubaPHP = $this->setupClass();
        $presents = $arubaPHP->getPresents(new \DateTime('2011-12-13 14:15'));
        return $this->assertEquals($presents,$this->getFixturePresents());

    }

    public function testGetEmployees()
    {
        $arubaPHP = $this->setupClass();
        $employees = $arubaPHP->getEmployees();
        return $this->assertEquals($employees,$this->getFixtureEmployees());
    }

    public function testGetEmployeeClocking()
    {
        $arubaPHP = $this->setupClass();
        $clocking = $arubaPHP->getEmployeeClocking('ABCDEF12G34H567I', new \DateTime('2016-11-12'), new \DateTime('2016-11-13'));
        return $this->assertEquals($clocking,$this->getFixtureClocking());
    }

    public function testGetEmployeeReasons()
    {
        $arubaPHP = $this->setupClass();
        $reasons = $arubaPHP->getEmployeeReasons('ABCDEF12G34H567I', new \DateTime('2012-12-12'), new \DateTime('2012-12-13'), -1);
        return $this->assertEquals($reasons,$this->getFixtureReasons());
    }

    private function setupClass()
    {
        $baseUrl = 'http://cloud.oltremare.net:3011/';
        $sito = "12|34";
        $azienda = "56|78";
        $client = new ClientCurlMockup();
        $arubaPHP = new ArubaPhp($baseUrl, $sito, $azienda, $client);
        return $arubaPHP;
    }

    private function getFixturePresents()
    {
        $presents = ['codice' => 'fiscale'];
        return $presents;
    }
    private function getFixtureEmployees()
    {
        $employees = ['codice' => 'fiscale'];
        return $employees;
    }
    private function getFixtureClocking()
    {
        $clocking = ['codice' => 'fiscale'];
        return $clocking;
    }
    private function getFixtureReasons()
    {
        $reasons = ['codice' => 'fiscale'];
        return $reasons;
    }
}
