<?php

use ArubaPhp\Client\AbstractClient;

class ClientCurlMockup extends AbstractClient
{

    /**
     * @param string $command
     * @param array $params
     * @return mixed
     */
    public function get($command, $params)
    {
        $base = [
            'sito' => $this->getSito(),
            'azicode' => $this->getAzienda(),
            'json' => 'true',
        ];

        $url = $this->getBaseUrl()
            . $command
            . '?' . str_replace('%7C', '|', http_build_query($base, null, '&', PHP_QUERY_RFC3986));

        if (count($params)) {
            $url .= '&' . str_replace('%7C', '|', http_build_query($params, null, '&', PHP_QUERY_RFC3986));
        }
        if ($url == 'http://cloud.oltremare.net:3011/LISTA_PRE?sito=12|34&azicode=56|78&json=true&data=13122011&ora=1415') {
            return $this->getJsonFixtureDecoded('LISTA_PRE');
        }
        return false;
    }

    /**
     * @param string $fixture
     * @return mixed
     */
    private function getJsonFixtureDecoded($fixture)
    {
        $rawJson = file_get_contents(__DIR__ . '/fixtures/' . $fixture . '.json');
        return json_decode($rawJson);
    }
}