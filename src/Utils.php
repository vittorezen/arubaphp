<?php

namespace ArubaPhp;

class Utils
{
    /**
     * @param string $giorno GG/MM/AAAA
     *
     * @return \DateTime
     *
     * Converte giorno in DateTime object
     */
    public static function convertDate($giorno)
    {
        list($gg, $mm, $yy) = explode('/', $giorno);

        return new \DateTime($yy.'-'.$mm.'-'.$gg.' 00:00:00');
    }

    /**
     * @param string $giorno GG/MM/AAAA
     * @param string $orario HH.II
     *
     * @return \DateTime
     *
     * Converte giorno ora in DateTime object
     */
    public static function convertDateTime($giorno, $orario)
    {
        list($gg, $mm, $yy) = explode('/', $giorno);
        list($hh, $ii) = explode('.', $orario);

        return new \DateTime($yy.'-'.$mm.'-'.$gg.' '.$hh.':'.$ii.':00');
    }
}
