<?php

namespace ArubaPhp\Client;

use ArubaPhp\ArubaPhp;

/**
 * Abstract AbstractClient.
 */
abstract class AbstractClient
{
    private $baseUrl;
    private $sito;
    private $azienda;

    /**
     * @param string $baseUrl
     * @param string $sito
     * @param string $azienda
     *
     *  set ArubaPhp
     */
    final public function init($baseUrl, $sito, $azienda)
    {
        $this->baseUrl = $baseUrl;
        $this->sito = $sito;
        $this->azienda = $azienda;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @return string
     */
    public function getSito()
    {
        return $this->sito;
    }

    /**
     * @return string
     */
    public function getAzienda()
    {
        return $this->azienda;
    }

    /**
     * @param string $command
     * @param array  $params
     */
    abstract public function get($command, $params);
}
