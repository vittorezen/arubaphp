<?php

namespace ArubaPhp\Client;

/**
 * Class ClientCurl.
 *
 * Curl implementation
 */
class ClientCurl extends AbstractClient
{
    /**
     * @param string $command
     * @param array  $params
     *
     * @return mixed
     */
    public function get($command, $params)
    {
        $base = [
            'sito' => $this->getSito(),
            'azicode' => $this->getAzienda(),
            'json' => 'true',
        ];

        $url = $this->getBaseUrl()
            .$command
            .'?'.str_replace('%7C', '|', http_build_query($base, null, '&', PHP_QUERY_RFC3986));

        if (count($params)) {
            $url .= '&'.str_replace('%7C', '|', http_build_query($params, null, '&', PHP_QUERY_RFC3986));
        }

        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $output = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);

        return json_decode($output, true);
    }
}
