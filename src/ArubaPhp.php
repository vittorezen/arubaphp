<?php

namespace ArubaPhp;

use ArubaPhp\Client\AbstractClient;
use ArubaPhp\Client\ClientCurl;
use ArubaPhp\Model\Dipendente;
use ArubaPhp\Model\Giustificativo;
use ArubaPhp\Model\Timbratura;

/**
 * Class ArubaPhp.
 */
class ArubaPhp
{
    /**
     * @var AbstractClient
     */
    private $client;

    /**
     * ArubaPhp constructor.
     *
     * @param $baseUrl string           If '' then use default http://cloud.oltremare.net:3011/
     * @param $sito    string           descrizione del sito | identificativo sito
     * @param $azienda string           codice dell’azienda (-1 = tutte) | identificativo azienda
     * @param $client  AbstractClient   If null use ClientCurl
     */
    public function __construct($baseUrl, $sito, $azienda, $client = null)
    {
        if ($baseUrl === '') {
            $baseUrl = 'http://cloud.oltremare.net:3011/';
        }
        if ($client === null) {
            $client = new ClientCurl();
        }
        $this->client = $client;

        // Initialize client with parameters
        $this->client->init($baseUrl, $sito, $azienda);
    }

    /**
     * @param \DateTime|null $dataOra
     *
     * @return array|null
     *
     * Permette di ottenere i presenti di un sito/azienda in un dato momento.
     *
     * http://IP_SERVER:3011/LISTA_PRE?sito=X|Y&azicode=Z|J&data=GGMMAAAA&ora=OOMM&json=true\r\n
     * -      sito: descrizione del sito | identificativo sito
     * -      azicode: codice dell’azienda (-1 = tutte) | identificativo azienda
     * -      data: GGMMAAAA
     * -      ora: HHMM
     * -      json: risposta con codifica JSON (“true” = codifica attiva)
     */
    public function getPresents($dataOra = null)
    {
        if ($dataOra === null) {
            $dataOra = new \DateTime();
        }

        $response = $this->client->get('LISTA_PRE', [
            'data' => $dataOra->format('dmY'),
            'ora' => $dataOra->format('Hi'),
        ]);

        if (!isset($response['dati'])) {
            return null;
        }

        $result = [];
        foreach ($response['dati'] as $cf => $p) {
            $result[$cf] = Timbratura::createFromRaw($p['Ultima transazione']['Verso'], $p['Ultima transazione']['Data'], $p['Ultima transazione']['Ora']);
        }

        return $result;
    }

    /**
     * @return array|null
     *
     * Permette di ottenere la lista dettagliata dei dipendenti (in forza) appartenenti al sito/azienda indicati.
     *
     * http://IP_SERVER:3011/LISTA_FULLDIP?sito=X|Y&azicode=Z|J&json=true\r\n
     * -      sito: descrizione del sito | identificativo sito
     * -      azicode: codice dell’azienda (-1 = tutte) | identificativo azienda
     * -      json: risposta con codifica JSON (“true” = codifica attiva)
     */
    public function getEmployees()
    {
        $response = $this->client->get('LISTA_FULLDIP', []);

        if (!isset($response['dati'])) {
            return null;
        }

        $result = [];
        foreach ($response['dati'] as $cf => $info) {
            $result[$cf] = new Dipendente($info);
        }

        return $result;
    }

    /**
     * @param string         $dipendente
     * @param \DateTime|null $dal
     * @param \DateTime|null $al
     *
     * @return array|null
     *
     * Permette di ottenere i dettagli del cartellino di un dipendente ( o tutti) per il giorno indicato.
     *
     * http://IP_SERVER:3011/LISTA_PRE?sito=X|Y&azicode=Z|J&$dip=KKK&dal=GGMMAAAA&al=GGMMAAAA&json=true\r\n
     * -      sito: descrizione del sito | identificativo sito
     * -      azicode: codice dell’azienda (-1 = tutte) | identificativo azienda
     * -      dip: codice fiscale del dipendente (-1 = tutti i dipendenti con codice fiscale significativo, che risultano in forza nel periodo selezionato)
     * -      dal: formato ggmmaaaa – es.: 01052016 (oggi = -1)
     * -      al: formato ggmmaaaa – es.: 01052016 (oggi = -1)
     * -      json: risposta con codifica JSON (“true” = codifica attiva)
     */
    public function getEmployeeClocking($dipendente, $dal = null, $al = null)
    {
        $response = $this->client->get('LISTA_CART', [
            'dip' => $dipendente,
            'dal' => ($dal === null ? '-1' : $dal->format('dmY')),
            'al' => ($al === null ? '-1' : $al->format('dmY')),
        ]);

        if (isset($response['dati'][$dipendente])) {
            $result = [];
            foreach ($response['dati'][$dipendente] as $v) {
                if (!isset($v['Giornaliero'])) {
                    continue;
                }

                foreach ($v['Giornaliero'] as $giorno => $vv) {
                    if (!isset($vv['Transazioni']) || $vv['Transazioni'] == '') {
                        continue;
                    }

                    foreach ($vv['Transazioni'] as $transazione) {
                        $timbratura = Timbratura::createFromRaw($transazione['Verso'], $giorno, $transazione['Orario'], $transazione['Causale'], $v['Azienda'], $dipendente);
                        $key = $timbratura->getDateTime()->format('YmdHi').'-'.uniqid();
                        $result[$key] = $timbratura;
                    }
                }
            }
            ksort($result);

            return $result;
        }

        return null;
    }

    /**
     * @param                $dipendente
     * @param \DateTime|null $dal
     * @param \DateTime|null $al
     * @param null           $tipo
     *
     * @return mixed
     *
     * Permette di ottenere i dettagli dei giustificativi di un dipendente (o tutti) nel periodo indicato.
     *
     * http://IP_SERVER:3011/LISTA_GIU?sito=X|Y&azicode=Z|J&$dip=KKK&dal=GGMMAAAA&al=GGMMAAAA&tipo=W&json=true\r\n
     * -      sito: descrizione del sito | identificativo sito
     * -      azicode: codice dell’azienda (-1 = tutte) | identificativo azienda
     * -      dip: codice fiscale del dipendente (-1 = tutti i dipendenti con codice fiscale significativo, che risultano in forza nel periodo selezionato)
     * -      dal: formato ggmmaaaa – es.: 01052016 (oggi = -1)
     * -      al: formato ggmmaaaa – es.: 01052016 (oggi = -1)
     * -      tipo: tipo giustificativo (-1 = tutti; T = autorizzati; R = rifiutati; F = pendenti)
     * -      json: risposta con codifica JSON (“true” = codifica attiva)
     */
    public function getEmployeeReasons($dipendente, $dal = null, $al = null, $tipo = null)
    {
        $response = $this->client->get('LISTA_GIU', [
            'dip' => ($dipendente === null ? '-1' : $dipendente),
            'dal' => ($dal === null ? '-1' : $dal->format('dmY')),
            'al' => ($al === null ? '-1' : $al->format('dmY')),
            'tipo' => ($tipo === null ? '-1' : $tipo),
        ]);

        if (!isset($response['dati']) || !is_array($response['dati'])) {
            return null;
        }

        $results = [];
        foreach ($response['dati'] as $date => $a) {
            foreach ($a[$dipendente] as $kb => $b) {
                foreach ($b['Giornaliero']['Giustificativi'] as $kc => $c) {
                    $results[$kb.'.'.$kc] = Giustificativo::createFromRaw($date, $c['Descrizione'], $c['Stato']);
                }
            }
        }

        return $results;
    }
}
