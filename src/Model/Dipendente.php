<?php

namespace ArubaPhp\Model;

use ArubaPhp\Attributes;

class Dipendente
{
    private $fields = [];

    /**
     * Dipendente constructor.
     *
     * @param array $fields
     */
    public function __construct(array $fields = [])
    {
        foreach ($fields as $name => $value) {
            $this->setField($name, $value);
        }
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setField($name, $value)
    {
        Attributes::checkFiledName($name);

        $this->fields[$name] = $value;
    }

    /**
     * @param string $name
     * @param null   $ifNotSet
     *
     * @return mixed|null
     */
    public function getField($name, $ifNotSet = null)
    {
        Attributes::checkFiledName($name);

        if (!isset($this->fields[$name])) {
            return $ifNotSet;
        }

        return $this->fields[$name];
    }
}
