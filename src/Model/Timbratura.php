<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 11/12/16
 * Time: 14:05.
 */

namespace ArubaPhp\Model;

use ArubaPhp\Utils;

class Timbratura
{
    const DIRECTION_IN = 'E';
    const DIRECTION_OUT = 'U';

    /**
     * @var string
     */
    private $direction;

    /**
     * @var \DateTime
     */
    private $dateTime;

    /**
     * @var string
     */
    private $reason;

    /**
     * @var string
     */
    private $customer;

    /**
     * @var string
     */
    private $employee;

    /**
     * @param string $direction
     * @param string $day
     * @param string $time
     * @param string $reason
     * @param string $customer
     * @param string $employee
     *
     * @return Timbratura
     */
    public static function createFromRaw($direction, $day, $time, $reason, $customer, $employee)
    {
        return new self($direction, Utils::convertDateTime($day, $time), $reason, $customer, $employee);
    }

    /**
     * Timbratura constructor.
     *
     * @param string    $direction
     * @param \DateTime $dateTime
     * @param string    $reason
     * @param string    $customer
     * @param string    $employee
     */
    public function __construct($direction, \DateTime $dateTime, $reason, $customer, $employee)
    {
        if ($direction == 'I') {
            $this->direction = static::IN;
        } elseif ($direction == 'O') {
            $this->direction = static::OUT;
        } else {
            $this->direction = $direction;
        }
        $this->dateTime = $dateTime;
        $this->reason = $reason;
        $this->customer = $customer;
        $this->employee = $employee;
    }

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string
     */
    public function getEmployee()
    {
        return $this->employee;
    }
}
