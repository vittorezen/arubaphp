<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 11/12/16
 * Time: 14:05.
 */

namespace ArubaPhp\Model;

use ArubaPhp\Utils;

class Giustificativo
{
    private $date;
    private $giustificativo;
    private $stato;

    /**
     * @param string $data           "13/12/2016"
     * @param string $giustificativo "Servizio"
     * @param string $stato          "Autorizzato"
     *
     * @return Timbratura
     */
    public static function createFromRaw($date, $giustificativo, $stato)
    {
        return new self(Utils::convertDate($date), $giustificativo, $stato);
    }

    /**
     * Timbratura constructor.
     *
     * @param string    $verso
     * @param \DateTime $dataOra
     */
    public function __construct(\DateTime $date, $giustificativo, $stato)
    {
        $this->date = $date;
        $this->giustificativo = $giustificativo;
        $this->stato = $stato;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getGiustificativo()
    {
        return $this->giustificativo;
    }

    /**
     * @return mixed
     */
    public function getStato()
    {
        return $this->stato;
    }
}
