<?php

namespace ArubaPhp;

class Attributes
{
    private static $fieldsName = [
        'Cognome',
        'Nome',
        'Sesso',
        'Luogo di nascita',
        'Prov. di nascita',
        'Data di nascita',
        'Codice fiscale',
        'IBAN',
        'Luogo di residenza',
        'Prov. di residenza',
        'Indirizzo di residenza',
        'CAP di residenza',
        'Luogo di domicilio',
        'Prov. di domicilio',
        'Indirizzo di domicilio',
        'CAP di domicilio',
        'Telefono',
        'Cellulare',
        'E-mail',
        'Azienda',
        'Reparto',
        'Qualifica',
        'Filiale',
        'Badge',
        'Badge alternativo',
        'Matricola',
        'Matricola paghe',
        'Posizione INAIL',
        'Data di assunzione',
        'Data di licenziamento',
        'Orario settimanale',
        'Gruppo di seq. orari sett.',
        'Gruppo Voci formula',
        'Costo orario',
        'Calendario',
        'Piano',
        'Stanza',
        'Telefono int.',
        'Elab. periodica',
        "Modalita' di timbratura",
        'Lavoro notturno',
        'Riclassificazione straordinari (per.)',
        'Valore orario',
        'Nota 1',
        'Nota 2',
        'Accesso come utente 1',
        'Accesso al menu cartellino',
        'Richiesta giustificativi',
        'Richiesta transazioni',
        'Richiesta straordinario',
        'Richiesta dal cartellino',
        'Anomalie',
        'Assegnazione aut. versi',
        'Messaggio',
        "Inizio validita'",
        "Fine validita'",
        'Percorso foto',
        'Giorni retribuiti',
        'Ore retribuite',
        'Giorni DM10',
        'Codice filiale',
        'Campo libero 1',
        'Campo libero 2',
        'Campo libero 3',
        'Campo libero 4',
        'Campo libero 5',
        'Contratto',
        'Sequenza orari gio.',
        'Squadra',
    ];

    /**
     * @return array
     */
    public static function getFieldList()
    {
        return self::$fieldsName;
    }

    /**
     * @param $name
     *
     * @throws \Exception
     */
    public static function checkFiledName($name)
    {
        if (!in_array($name, self::$fieldsName)) {
            throw new \Exception("Field '$name' not exist");
        }
    }
}
