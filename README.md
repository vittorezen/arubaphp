
#Aruba.php

[![Build Status](http://jenkins.consorzisdb.it:8080/buildStatus/icon?job=arubaPHP)](http://jenkins.consorzisdb.it:8080/job/arubaPHP/)

A simple Aruba.php (Oltremare SRL) wrapper.

It's symfony bundle ready.

## Installation

```
composer require vittorezen/arubaPHP
```

## Usage

```php
use ArubaPHP\Wrapper;
```


